const MercadoPago = require('mercadopago');

const getFullUrl = (req) =>{
    const url = req.protocol + '://' + req.get('host');
    console.log(url)
    return url;
}

module.exports = {
    async checkout(req, res){

        console.log(process.env)
        MercadoPago.configure({
            sandbox: process.env.SANDBOX == 'true' ? true : false,
            access_token: process.env.MP_ACCESS_TOKEN
        });

        const { id, email, description, amount } = req.params;

        //Create purchase item object template
        const purchaseOrder = {
            items: [
              item = {
                id: id,
                title: description,
                description : description,
                quantity: 1,
                currency_id: 'BRL',
                unit_price: parseFloat(amount)
              }
            ],
            payer : {
              email: email
            },
            auto_return : "all",
            external_reference : id,
            back_urls : {
              success : getFullUrl(req) + "/payments/success",
              pending : getFullUrl(req) + "/payments/pending",
              failure : getFullUrl(req) + "/payments/failure",
            }
          }
         
          //Generate init_point to checkout
          try {
            const preference = await MercadoPago.preferences.create(purchaseOrder);
            //preference.body.notification_url = 'http://www.lojamodelo.com.br/notificacao'
            console.log(preference)
            return res.redirect(`${preference.body.init_point}`);
          }catch(err){
            return res.send(err.message);
          }
    },
    async notification(req, res){

      console.log(process.env)
      MercadoPago.configure({
          sandbox: process.env.SANDBOX == 'true' ? true : false,
          access_token: process.env.MP_ACCESS_TOKEN
      });
      
      const { id, email, description, amount } = req.params;

      var payment = {
        description: description,
        transaction_amount: parseFloat(amount),
        payment_method_id: id,
        payer: {
          email: email,
          identification: {
            type: 'DNI',
            number: '34123123'
          }
        }
      };
      
      mercadopago.payment.create(payment, {
        access_token: 'TEST-2755024461161692-121310-79a1fab28d87aa09887454b4d793a54b-500102292',
      }).then(function (mpResponse) {
      });
      
    }
}

